import requests
import sys
import logging
import time
import os


##### Default Logging Configuration

slogformat = "%(asctime)s | %(levelname)s | %(message)s"
logging.basicConfig(level=logging.INFO, format=slogformat, stream=sys.stdout)
logging.getLogger("requests").setLevel(logging.INFO)

class Jenkins:

    def __init__(self, host, port=8086, token=None, jenkinsID=None, password=None, ssl_verify=True):

        self.jenkinID = jenkinsID
        self.password = password
        self.host = host
        self.port = port
        self.token = token

        self.jenkinsAddress = self.host+":"+str(self.port)
        self.defaultJobPath = None
        self.verify = ssl_verify

    def registerJob(self, jobPath):

        """
        set default job
        jobPath -> `/job/test_job/job/test_job/` in `http://host:port/job/test_job/job/test_job/`
            Note -> Starting and ending slash is important in jobPath
        """
        self.defaultJobPath = jobPath
        logging.info("Default job set to -> "+jobPath)

    def getCrumb(self):
        if (self.jenkinID and self.password):
            cred = (self.jenkinID, self.password)
        else:
            cred = None
        crumbRequest = requests.get(self.jenkinsAddress+"/crumbIssuer/api/json", auth=cred, verify=self.verify)

        try:
            crumbRequest.raise_for_status()
        except Exception as ex:
            logging.error("Crumb Generation Failed -> "+str(ex))
            return False

        logging.info("Crumb Generated -> "+str(crumbRequest.status_code))

        crumbData = crumbRequest.json()
        return crumbData

    def getCompleteJobPath(self, jobPath=None):

        """
        Return Complete Job URI
        jobPath -> `/job/test_job/job/test_job/` in `http://host:port/job/test_job/job/test_job/`
        Note -> 1) Starting and ending slash is important in jobPath
                2) if jobPath is `None` then use registerJob method on starting to define default job path
        """

        if(not jobPath):
            jobPath = self.defaultJobPath

        return self.jenkinsAddress+jobPath


    def triggerJob(self, parameters=None, jobPath=None, returnBuildInfo=False):
        """
            Trigger New Job
            jobPath -> `/job/test_job/job/test_job/` in `http://host:port/job/test_job/job/test_job/`
            parameters -> in case of build parameters you pass dictionary of parameters
            Note -> 1) Starting and ending slash is important in jobPath
            2) if jobPath is `None` then use registerJob method on starting to define default job path
        """

        if not jobPath:
            jobPath = self.defaultJobPath

        url = self.getCompleteJobPath(jobPath)

        if parameters:
            url = url+"buildWithParameters"
        else:
            url = url+"build"
            parameters = dict()

        crumb = self.getCrumb()

        if self.token:
            parameters['token'] = self.token

        if self.jenkinID and self.password:
            cred = (self.jenkinID, self.password)
        else:
            cred = None

        header = {crumb['crumbRequestField']:crumb['crumb']}

        newRequest = requests.post(url, auth=cred, headers=header, params=parameters, verify=self.verify)

        try:
            newRequest.raise_for_status()
        except Exception as ex:
            logging.error("Build Triggered Failed -> "+str(ex))
            return False

        logging.info("Build Triggered -> "+str(newRequest.status_code))

        if returnBuildInfo:

            time.sleep(10)

            buildInfo = self.getBuildInfo()

            return buildInfo

        return True

    def getBuildInfo(self, jobPath=None, buildNo=None):
        """
        get build information
        :param buildNo: Build Number, if `None` it will return info for last Build
        :return: detail of build as dict on success and False on Failure

        jobPath -> `/job/test_job/job/test_job/` in `http://host:port/job/test_job/job/test_job/`
            Note -> 1) Starting and ending slash is important in jobPath
            2) if jobPath is `None` then use registerJob method on starting to define default job path

        """
        if not jobPath:
            jobPath = self.defaultJobPath
        if not buildNo:
            buildNo = "lastBuild"
        buildNo = str(buildNo)

        url = self.getCompleteJobPath(jobPath)
        url = url+buildNo+"/api/json"
        crumb = self.getCrumb()

        if self.token:
            url = url+"?token="+self.token

        if self.jenkinID and self.password:
            cred = (self.jenkinID, self.password)
        else:
            cred=None
        header = {crumb['crumbRequestField']: crumb['crumb']}

        newBuildInfoRequest = requests.get(url, auth=cred, headers=header, verify=self.verify)

        try:
            newBuildInfoRequest.raise_for_status()
        except Exception as ex:
            logging.error("Unable to get Build Info -> "+str(ex))
            return False

        logging.info("Got Build Info -> "+str(newBuildInfoRequest.status_code))
        return dict(newBuildInfoRequest.json())

    def getJobResult(self, jobPath=None, buildNo=None, waitForJobToComplete=True):
        """
        return status or result of given Job
        :param buildNo: Build Number, if `None` it will return info for last Build
        :param jobPath: `/job/test_job/job/test_job/` in `http://host:port/job/test_job/job/test_job/` Note -> 1) Starting and ending slash is important in jobPath 2) if jobPath is `None` then use registerJob method on starting to define default job path
        :param waitForJobToComplete: if `True` then it will wait for job to complete then return final result. if `False` then  return current status or result.
        :return: Status -> str
        """
        buildInfo = self.getBuildInfo(jobPath=jobPath, buildNo=buildNo)

        if not buildInfo:
            return False

        buildingStatus = buildInfo['building']
        buildNo = buildInfo['number']
        if(buildingStatus and waitForJobToComplete):
            time.sleep(10)
            return self.getJobResult(jobPath, buildNo, waitForJobToComplete)

        return buildInfo['result']

    def getJobConsoleTextPath(self, buildNo=None):

        if not buildNo:
            buildNo = "lastBuild"
        buildNo = str(buildNo)

        return self.getCompleteJobPath()+buildNo+"/consoleText"

    def createMsg(self, buildResult, buildNo, jobPath=None, logfile="N/A"):

        if not jobPath:
            jobPath = self.defaultJobPath

        logLink = self.getJobConsoleTextPath(buildNo)

        msg = "Build Job -> {}\n\nBuild Number -> {}\n\nBuild Result -> {}\n\nBuild Log -> {}\n\n"
        msg = msg.format(jobPath, buildNo, buildResult, logLink)

        if buildResult == "FAILURE":

            log = self.getTextFromCosoleTextLog(buildNo)
            logError = log.splitlines()[-2]
            msg = msg+"{}\n\n".format(logError.replace(":", " ->"))

        msg = msg+"Attachment -> {}".format(logfile)

        return msg

    def getTextFromCosoleTextLog(self, buildNo=None):

        log_url = self.getJobConsoleTextPath(buildNo)

        r = requests.get(url=log_url, verify=self.verify)

        try:
            r.raise_for_status()
        except Exception as e:
            logging.error("Unable to get text from log --> "+e)
            return False

        return r.text

    def create_temp_log_file(self, buildNo, jobPath=None):

        if not jobPath:
            jobPath = self.defaultJobPath

        log = self.getTextFromCosoleTextLog(buildNo)

        jobName = jobPath.split("/")[-2]

        logFileName = "temp/{}_{}.txt".format(jobName, buildNo)

        fileObj = open(logFileName, "w")
        fileObj.write(log)
        fileObj.close()

        return logFileName

    def del_temp_log_file(self, logFilePath):

        if os.path.exists(logFilePath):
            os.remove(logFilePath)